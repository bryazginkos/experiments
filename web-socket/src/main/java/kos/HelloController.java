package kos;

import kos.model.Information;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kos on 17.06.15.
 */
@RestController
public class HelloController {

    @RequestMapping(value = "/npe", method = RequestMethod.GET)
    public String error() {
        throw new NullPointerException();
    }

    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    public String hello() {
        return "Hello world";
    }

    @RequestMapping(value = "/yaya", method = RequestMethod.GET, produces = "application/json")
    public Information info() {
        return new Information("12", "twelve");
    }

}
