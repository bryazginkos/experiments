package kos;

import kos.model.FromMessage;
import kos.model.ToMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kos on 27.06.15.
 */
@Controller
public class SimpleController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/websocketservice")
    public void greeting(FromMessage message) throws Exception {
        Thread.sleep(3000); // simulated delay
        messagingTemplate.convertAndSend("/topic/to", new ToMessage(message.getMessage()));
    }

    @RequestMapping("/testing")
    public String startPage() {
        return "first";
    }

    @Scheduled(fixedDelay = 4000)
    public void showTime() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        messagingTemplate.convertAndSend("/topic/to", new ToMessage(sdf.format(date)));
    }
}
