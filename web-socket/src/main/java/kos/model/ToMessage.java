package kos.model;

/**
 * Created by kos on 27.06.15.
 */
public class ToMessage {

    private String content;

    public ToMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
