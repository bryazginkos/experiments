package kos.model;

/**
 * Created by kos on 27.06.15.
 */
public class FromMessage {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
