package kos.model;

/**
 * Created by kos on 17.06.15.
 */
public class Information {
    private String id;
    private String title;

    public Information(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
