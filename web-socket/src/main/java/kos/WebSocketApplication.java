package kos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by kos on 27.06.15.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class WebSocketApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(WebSocketApplication.class, args);
    }
}
